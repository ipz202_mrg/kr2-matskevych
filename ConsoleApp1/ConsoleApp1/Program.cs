﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace DijkstraAlgorithm
{
    class DEKSTR
    {

        private static int MinimumDistance(int[] distance, bool[] sh, int Count)
        {
            int min = int.MaxValue;
            int min1 = 0;

            for (int i = 0; i < Count; i++)
            {
                if (sh[i] == false && distance[i] <= min)
                {
                    min = distance[i];
                    min1 = i;
                }
            }

            return min1;
        }

        private static void Print(int[] distance, int Count)
        {
            Console.WriteLine("Вершина    Відстань від початку");

            for (int i = 0; i < Count; i++)
                Console.WriteLine("{0}\t  {1}", i, distance[i]);
        }

        public static void DijkstraAlgo(int[,] graph, int source, int Count)
        {
            int[] distance = new int[Count];
            bool[] shortestPathTreeSet = new bool[Count];

            for (int i = 0; i < Count; ++i)
            {
                distance[i] = int.MaxValue;
                shortestPathTreeSet[i] = false;
            }

            distance[source] = 0;

            for (int count = 0; count < Count - 1; count++)
            {
                int a = MinimumDistance(distance, shortestPathTreeSet, Count);
                shortestPathTreeSet[a] = true;

                for (int j = 0; j < Count; j++)
                    if (!shortestPathTreeSet[j] && Convert.ToBoolean(graph[a, j]) && distance[a] != int.MaxValue && distance[a] + graph[a, j] < distance[j])
                        distance[j] = distance[a] + graph[a, j];
            }

            Print(distance, Count);
        }

        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            int[,] graph =  {
                         { 0, 7, 0, 0, 0, 0, 0, 9, 0 },
                         { 6, 0, 9, 0, 0, 0, 0, 11, 0 },
                         { 0, 9, 0, 5, 0, 6, 0, 0, 2 },
                         { 0, 0, 5, 0, 9, 16, 0, 0, 0 },
                         { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                         { 0, 0, 6, 0, 10, 0, 2, 0, 0 },
                         { 0, 0, 0, 16, 0, 2, 0, 1, 6 },
                         { 9, 11, 0, 0, 0, 0, 1, 0, 5 },
                         { 0, 0, 2, 0, 0, 0, 6, 5, 0 }
                            };

            DijkstraAlgo(graph, 0, 9);
            Console.ReadKey();
        }
    }
}